FROM node

RUN mkdir /app
WORKDIR /app

COPY package.json /app
WORKDIR /app
RUN yarn install

COPY . /app
RUN yarn test
RUN yarn build
CMD yarn start

EXPOSE 3000





#FROM node
#
#RUN mkdir /app
#COPY . /app
#WORKDIR /app
#
#RUN yarn install
#RUN yarn test
#RUN yarn build 
#
#CMD yarn start
#
#EXPOSE 3000


